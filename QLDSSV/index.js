var dssv = [];
BASE_URL = "https://62ff962c34344b6431fcdd0c.mockapi.io"
var turnOnLoading = function(){
  document.getElementById("loading").style.display="flex";
};

var turnOffLoading = function(){
  document.getElementById("loading").style.display="none"
};

var renderTable = function(list){
  var contentHTML = "";
  list.forEach(function(item){
    var trContent = `
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td><img src=${item.hinhAnh} style="width:100px" alt=""/></td>
    <td>
    <button class="btn btn-danger" onclick= "xoaSinhVien('${item.ma}')">Xoá</button>
    <button class="btn btn-warning" onclick= "suaSinhVien('${item.ma}')">Sửa</button>
    </td>
    </tr>
    `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML=contentHTML;
};

var renderDSSVService = function (){
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv`, 
    method:"GET",
  })
  .then(function(res){
    turnOffLoading();
    dssv = res.data;
    renderTable(dssv);
    // console.log(dssv);
  })
  .catch(function(err){
    turnOffLoading();
    console.log(err);
  });
};
renderDSSVService();

//Xoá sinh viên
function xoaSinhVien(id){
  turnOnLoading();
  axios({
    url :`${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
  .then(function(res){
    turnOffLoading();
    //chạy lại renderDSSVService để render danh sách mới nhất
    renderDSSVService();
console.log(res);
  })
  .catch(function(err){
    turnOffLoading();
console.log(err);
  });
}

//Thêm sinh viên
function themSV(){
  var dataForm = layThongTinTuForm();
  turnOnLoading();
  axios({
    url: `${BASE_URL}}/sv`, 
    method : "POST",
    data : dataForm,
  })
  .then(function(res){
    turnOffLoading();
    renderDSSVService();
    console.log(res);

  })
  .catch(function(){
    turnOffLoading();
console.log(err);

  })
}

//Sửa sinh viên
function suaSinhVien(id){
  console.log('id: ', id);
axios({
  url : `${BASE_URL}/sv/${id}`,
  method : "GET"
})
.then(function(res){
  showThongTinLenForm(res.data);
console.log(res);
})
.catch(function(err){
console.log(err);
})
};

//Cập nhật sinh viên
function capNhatSV(){
  var dataForm = layThongTinTuForm();
  console.log('dataForm: ', dataForm);
  turnOnLoading();
axios ({
  url : `${BASE_URL}/sv/${dataForm.ma}`,
  method : "PUT",
  data : dataForm,
})
.then(function(res){
  turnOffLoading();
  renderDSSVService();
console.log(res);
})
.catch(function(err){
  turnOffLoading();
console.log(err);
});
}